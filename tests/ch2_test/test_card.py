import allure
import pytest

from cards import Card
from hamcrest import assert_that, equal_to
from src.helpers.asserting import Asserting


@allure.tag("POSITIVE")
def test_failed_check():
    c = Card("something", "brian", "todo", 123)
    Asserting.equal_values(
        values=[c.summary, c.owner, c.state, c.id],
        values_to_check=["something", "brian", "todo", 123]
    )


@allure.tag("POSITIVE")
def test_defaults():
    c = Card()
    Asserting.equal_values(
        values=[c.summary, c.owner, c.state, c.id],
        values_to_check=[None, None, "todo", None]
    )


@allure.tag("POSITIVE")
def test_equality_objects():
    c1 = Card("something", "brian", "todo", 123)
    c2 = Card("something", "brian", "todo", 123)
    assert_that(c1, equal_to(c2))
    Asserting.equal_values(values=[c1], values_to_check=[c2])


@allure.tag("POSITIVE")
def test_equality_diffs_ids():
    c1 = Card("something", "brian", "todo", 123)
    c2 = Card("something", "brian", "todo", 113)
    Asserting.equal_values(values=[c1], values_to_check=[c2])


@allure.tag("POSITIVE")
def test_inequality():
    c1 = Card("something", "brian", "todo", 123)
    c2 = Card("completely different", "okken", "done", 123)
    Asserting.not_equal_values(values=[c1], values_to_check=[c2])


@allure.tag("POSITIVE")
def test_from_dict():
    c1 = Card("something", "brian", "todo", 123)
    c2_dict = {
        "summary": "something",
        "owner": "brian",
        "state": "todo",
        "id": 123,
    }
    c2 = Card.from_dict(c2_dict)
    Asserting.equal_values(values=[c1], values_to_check=[c2])


@allure.tag("POSITIVE")
def test_to_dict():
    c1 = Card("something", "brian", "todo", 123)
    c2 = c1.to_dict()
    c2_expected = {
        "summary": "something",
        "owner": "brian",
        "state": "todo",
        "id": 123,
    }
    Asserting.equal_values(values=[c2], values_to_check=[c2_expected])


@allure.tag("POSITIVE")
@pytest.mark.xfail
def test_equality_fail():
    c1 = Card("sit there", "brian")
    c2 = Card("do something", "okken")
    Asserting.equal_values(values=[c1], values_to_check=[c2])
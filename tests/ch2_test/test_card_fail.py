import cards
import pytest
import allure


@allure.tag("NEGATIVE")
def test_no_path_fail():
    match_regex = "missing 1 .* positional argument"
    with pytest.raises(TypeError, match=match_regex):
        cards.CardsDB()



from hamcrest import assert_that, equal_to


def test_passing():
    assert_that(
        (1, 2, 3),
        equal_to((1, 2, 3)),
        "Not equal"
    )

from hamcrest import assert_that, equal_to, not_


class Asserting:
    @staticmethod
    def equal_values(values: list, values_to_check: list):
        for value in values:
            index_value = values.index(value)
            check_value = values_to_check[index_value]
            assert_that(value, equal_to(check_value), "Values are not equal")

    @staticmethod
    def not_equal_values(values: list, values_to_check: list):
        for value in values:
            index_value = values.index(value)
            check_value = values_to_check[index_value]
            assert_that(value, not_(equal_to(check_value)), "Values do not have to be equal")
FROM python:3.11-slim
ENV PATH="$PATH:/root/.local/bin"

RUN apt-get update && apt-get install -y curl jq  \
    && curl -sSL https://install.python-poetry.org | python3docker

COPY pyproject.toml .
RUN poetry config virtualenvs.create false && poetry install
import pytest
import time


@pytest.fixture(autouse=True)
def duration():
    start = time.time()
    yield
    end = time.time()
    print(f"\n Время выполнения: {end - start} секунд")